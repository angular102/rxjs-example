import {Observable,fromPromise, fromEvent, of, interval, timer, range, from} from "rxjs";
import {
    filter,
    map,
    scan,
    take,
    pluck,
    first,
    last,
    find,
    findIndex,
    skip,
    skipWhile,
    takeWhile,
    skipUntil,
    takeUntil
} from 'rxjs/operators'

function createSubscribeTemplate(name) {
    return {
        next(value) {
            console.log(value)
        },
        error(err) {
            console.log(`Error: ${err}`)
        },
        complete() {
            console.log(`${name}: Completed !`)
        }
    }
}

// fromEvent(document.querySelector('.input-type-text'), 'keyup')
//     .pipe(
//         pluck('target', 'value'),
//         // map(value => value.target.value),
//         map(value => value.toUpperCase()),
//         map(value => {
//             return {
//                 value: value,
//                 lenght: value.length
//             }
//         })
//     )
//     .subscribe(
//         createSubscribeTemplate('map')
//     )



// of('Hello', 'world', 1, 5)
//     .pipe(
//         // first()
//         // last()
//         // find(value => {
//         //     if (typeof x === "string") {
//         //         return value.toLowerCase() === 'hello'
//         //     }
//         // })
//         // findIndex(value => value === 0)
//         // take(2)
//         // skip(2)
//         // skipWhile(value => {
//         //     return typeof value === 'string'
//         // })
//
//     )
//     .subscribe(createSubscribeTemplate('find '))



// interval(500)
//     .pipe(
//         skipWhile(value => value < 5),
//         // take(10)
//         takeWhile(value => value < 13)
//     )
//     .subscribe(createSubscribeTemplate('skipWhile'))


interval(500)
    .pipe(
        skipUntil(timer(5000)),
        takeUntil(timer(9000))
        // take(10)
    )
    .subscribe(createSubscribeTemplate('skipWhile'))





