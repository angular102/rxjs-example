import {Observable,fromPromise, fromEvent, of, interval, timer, range, from} from "rxjs";
import {
    filter,
    map,
    scan,
    take,
    pluck,
    first,
    last,
    find,
    findIndex,
    skip,
    skipWhile,
    takeWhile,
    skipUntil,
    takeUntil, debounceTime, distinct, distinctUntilChanged, buffer, bufferTime, bufferCount
} from 'rxjs/operators'

function createSubscribeTemplate(name) {
    return {
        next(value) {
            console.log(value)
        },
        error(err) {
            console.log(`Error: ${err}`)
        },
        complete() {
            console.log(`${name}: Completed !`)
        }
    }
}


// range(0,40)
//     .pipe(
//         // bufferTime(2000),
//         // buffer(interval(200)),
//         // take(4)
//         bufferCount(10)
//
//     )
//     .subscribe(
//         createSubscribeTemplate('bufferTime')
//     )


interval(1000)
    .pipe(
        buffer(
            fromEvent(document, 'click')
        ),
        map(x => x.length)
    )
    .subscribe(
        createSubscribeTemplate('buffer')
    )