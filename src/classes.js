import {Observable, Subject, BehaviorSubject,
    ReplaySubject,AsyncSubject,
    fromPromise, fromEvent,
    merge, of, interval, timer,
    range,
    from} from "rxjs";
import {
    filter,
    map,
    scan,
    take,
    pluck,
    first,
    last,
    find,
    findIndex,
    skip,
    skipWhile,
    takeWhile,
    skipUntil,
    takeUntil, debounceTime, distinct, distinctUntilChanged, buffer,
    bufferTime, bufferCount, defaultIfEmpty, every, tap,
    delay, mergeMap, concatMap, zip, withLatestFrom,
    combineLatest, catchError,
} from 'rxjs/operators'



function createSubscribeTemplate(name) {
    return {
        next(value) {
            console.log(value)
        },
        error(err) {
            console.log(`Error: ${err}`)
        },
        complete() {
            console.log(`${name}: Completed !`)
        }
    }
}



const subject$ = new Subject();
// subject$.subscribe(
//     createSubscribeTemplate('subject')
// );
// subject$.next(1)
// subject$.next(2)
//
// setTimeout(() => {
//     subject$.next(3)
//     subject$.complete()
// }, 3000)

// const int$ = interval(1000)
// int$.subscribe(subject$)
// subject$.subscribe(createSubscribeTemplate('subject 1'))
// subject$.subscribe(createSubscribeTemplate('subject 2'))
//
// setTimeout(() => {
//     subject$.subscribe(createSubscribeTemplate('subject 3'))
// }, 2000)


// const subjectBehavior$ = new BehaviorSubject('BTC')
// subjectBehavior$
//     .subscribe(
//     createSubscribeTemplate('behavior')
// )
// subjectBehavior$.next('Binance')
// subjectBehavior$.complete()




// const subjectReplay$ = new ReplaySubject(2)
//
// subjectReplay$.next(1)
// subjectReplay$.next(2)
// subjectReplay$.next(3)
// subjectReplay$.complete()
//
// subjectReplay$
//     .subscribe(
//         createSubscribeTemplate('replay')
//     )










const subjectAsync$ = new AsyncSubject()

subjectAsync$.next(1)
subjectAsync$.next('BTC')
subjectAsync$.complete()


subjectAsync$
    .subscribe(
        createSubscribeTemplate('async')
    )