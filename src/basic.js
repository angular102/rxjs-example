import {Observable, fromEvent, of, interval, timer, range, from} from "rxjs";
import {filter, map, scan, take} from 'rxjs/operators'


// let stream$ = new Observable( observer => {
//     observer.next('One')
// });
//
// stream$.subscribe(value => {
//     console.log(`Subscribe: ${value}`)
// });


// let stream$ = new Observable( observer => {
//     console.log(`Stream$ was created`)
//     observer.next('One')
//
//     setTimeout(() => {
//         observer.next('After 2 seconds')
//     }, 2000)
//
//     // setTimeout(() => {
//     //     observer.complete()
//     // }, 3000)
//
//     setTimeout(() => {
//         observer.error('Something went wrong')
//     }, 3100)
//
//     observer.next('Two')
// });
// stream$.subscribe(
//     (val) => console.log('Value:', val),
//     (err) => console.error(`Error: ${err}`),
//     () => console.log('Completed !')
// );


// let btn$ = fromEvent(document.querySelector('.btn-test-click'), 'click')
// btn$.subscribe(
//     (e) => console.log(e)
// )
//
// fromEvent(document.querySelector('.input-type-text'), 'keyup')
//     .subscribe(
//         (e) => console.log(e)
//     )
//
//
// fromEvent(document, 'mousemove')
//     .subscribe(
//         (e) => {
//             document.querySelector('.mousemove-test').innerHTML = `X: ${e.clientX}, Y: ${e.clientY}`
//         }
//     )






// of(1,2,3,4,56,'BNB','BTC', [57, 67,'INC'])
//     .subscribe(
//         (value) => console.log(`Next: ${value}`),
//         (error) => console.error(`Error: ${error}`),
//         () => console.log('Completed !')
//     )

function createSubscribeTemplate(name) {
    return {
        next(value) {
            console.log(`${name}: ${value}`)
        },
        error(err) {
            console.log(`Error: ${err}`)
        },
        complete() {
            console.log(`${name}: Completed !`)
        }
    }
}

// of(1,2,3,4,56,'BNB','BTC', [57, 67,'INC'])
//     .subscribe(
//         createSubscribeTemplate('of')
//     )

// interval(300)
//     .pipe(
//         take(15)
//     )
//     .subscribe(createSubscribeTemplate('interval'))

// timer(5000, 500)
//     .pipe(
//         take(10)
//     )
//     .subscribe(createSubscribeTemplate('Timer'))


// range(3, 6)
//     .subscribe(createSubscribeTemplate('Timer'))


// let arr$ = from([1,2,3,4])
// arr$.subscribe(
//     createSubscribeTemplate('From')
// )


// const set = new Set([1,2,3,4,'5','6',{id: 7}])
// let arr$ = from(set)
// arr$.subscribe(
//     createSubscribeTemplate('From')
// )

const maps = new Map([[1,2], [3,4], [5,6]])
let arr$ = from(maps)
arr$.subscribe(
    createSubscribeTemplate('From')
)


