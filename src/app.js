import {Observable,from,fromPromise,of,fromEvent} from "rxjs";
import {pluck, debounceTime, distinct,mergeMap,catchError, merge,map,mergeAll} from 'rxjs/operators'
import axios from 'axios'

function getUsersById(id) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const data = axios.get(`https://jsonplaceholder.typicode.com/users?id=${id}`)
            resolve(data);
        }, 0)
    })
}

fromEvent(document.querySelector('.input-api'), 'keyup')
    .pipe(
        pluck('target', 'value'),
        distinct(),
        debounceTime(500),
        mergeMap(param => from(getUsersById(param))),
        catchError(err => of(err)),
        map(x => x.data[0])
    )
    .subscribe(
        (user) => {
            document.querySelector('h1').innerHTML = ''
            if (user.name) {
                document.querySelector('h1').innerHTML = `
                ${user.name} ${user.username} ${user.email}
            `
            }
        },
        (error) => console.error(error),
        () => console.log('Completed')
    )
