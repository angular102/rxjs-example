import {Observable,fromPromise, fromEvent, merge, of, interval, timer, range, from} from "rxjs";
import {
    filter,
    map,
    scan,
    take,
    pluck,
    first,
    last,
    find,
    findIndex,
    skip,
    skipWhile,
    takeWhile,
    skipUntil,
    takeUntil, debounceTime, distinct, distinctUntilChanged, buffer,
    bufferTime, bufferCount, defaultIfEmpty, every, tap, delay, mergeMap, concatMap, zip, withLatestFrom, combineLatest,
} from 'rxjs/operators'
import {pipe} from "rxjs/dist/types";


function createSubscribeTemplate(name) {
    return {
        next(value) {
            console.log(value)
        },
        error(err) {
            console.log(`Error: ${err}`)
        },
        complete() {
            console.log(`${name}: Completed !`)
        }
    }
}


// const s1$ = of('Hello')
// const s2$ = of('Ivan')

// s1$.merge(s2$)
// .subscribe(
//     createSubscribeTemplate('merge')
// )

// merge(s1$, s2$)
//     .subscribe(createSubscribeTemplate('merge'))

// of('Hello').subscribe(
//     x => {
//         of(x + ' world').subscribe(
//             createSubscribeTemplate('mergeMap')
//         )
//     }
// )


// of('BTC')
//     .pipe(
//         mergeMap(x => {
//             return of(x + ' Binance')
//         })
//     ).subscribe(
//         createSubscribeTemplate('mergeMap')
// )




// const promise = (data) => {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             resolve(data + ' Binance cool !')
//         }, 2000)
//     })
// }
//
// of('BTC')
//     .pipe(
//         mergeMap((x) => {
//             return promise(x)
//         })
//     )
//     .subscribe(
//         createSubscribeTemplate('promise')
//     )


const s1$ = of('Hello')
const s2$ = of('Ivan')

s1$.pipe(zip(s2$)).subscribe(createSubscribeTemplate('zip'))

const int1$ = interval(1000)
const int2$ = interval(500)
int1$
    .pipe(
    withLatestFrom(int2$),
    take(5)
)
.subscribe(createSubscribeTemplate('widthLatestFrom'))


const timer1$ = timer(1000, 2000)
const timer2$ = timer(2000, 2000)
const timer3$ = timer(3000, 2000)

pipe(combineLatest(timer1$, timer2$, timer3$))


