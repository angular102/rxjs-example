import {Observable,fromPromise, fromEvent, of, interval, timer, range, from} from "rxjs";
import {
    filter,
    map,
    scan,
    take,
    pluck,
    first,
    last,
    find,
    findIndex,
    skip,
    skipWhile,
    takeWhile,
    skipUntil,
    takeUntil, debounceTime, distinct, distinctUntilChanged, buffer,
    bufferTime, bufferCount, defaultIfEmpty, every, tap, delay,
} from 'rxjs/operators'
import {observable} from "rxjs/dist/types";

function createSubscribeTemplate(name) {
    return {
        next(value) {
            console.log(value)
        },
        error(err) {
            console.log(`Error: ${err}`)
        },
        complete() {
            console.log(`${name}: Completed !`)
        }
    }
}


// of(1)
//     .pipe(
//         defaultIfEmpty('I am empty stream')
//     )
//     .subscribe(
//         createSubscribeTemplate('of')
//     )


// from([1,2,3,4,5])
//     .pipe(
//         // map(x => x*2),
//         skipWhile(x => x <= 3),
//         every(x => x > 2)
//     )
//     .subscribe(
//         createSubscribeTemplate('every')
//     )


// from([1,2,3,4,5])
//     .pipe(
//        tap(x => {
//            console.log(`Before: ${x}`)
//        }),
//         map(x => x * x),
//         tap(x => {
//             console.log(`After: ${x}`)
//         }),
//     )
//     .subscribe(
//         createSubscribeTemplate('tap')
//     )


// from([1,2,3,4,5])
//     .pipe(
//         map(x => x * x),
//         delay(3000),
//     )
//     .subscribe(
//         createSubscribeTemplate('delay')
//     )

