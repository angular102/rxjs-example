import {Observable,fromPromise, fromEvent, of, interval, timer, range, from} from "rxjs";
import {
    filter,
    map,
    scan,
    take,
    pluck,
    first,
    last,
    find,
    findIndex,
    skip,
    skipWhile,
    takeWhile,
    skipUntil,
    takeUntil, debounceTime, distinct, distinctUntilChanged
} from 'rxjs/operators'

function createSubscribeTemplate(name) {
    return {
        next(value) {
            console.log(value)
        },
        error(err) {
            console.log(`Error: ${err}`)
        },
        complete() {
            console.log(`${name}: Completed !`)
        }
    }
}


// range(0, 10)
//     .pipe(
//         filter(value => value > 4)
//     )
//     .subscribe(createSubscribeTemplate('filter'))


// const cars = [
//     {
//         name: 'Audi',
//         price: 500
//     },
//     {
//         name: 'BMW',
//         price: 400
//     },
//     {
//         name: 'ford',
//         price: 200
//     }
// ]

// fromEvent(document.querySelector('.input-type-text'), 'keyup')
//         .pipe(
//             map(e => e.target.value)
//         )
//         .subscribe(
//             (x) => {
//                 from(cars)
//                     .pipe(
//                         filter(c => c.name === x)
//                     )
//                     .subscribe(
//                         (v) => {
//                             document.querySelector('.cars_inner')
//                                 .innerHTML = `
//                                     <h2>${v.name.toUpperCase()}</h2>
//                                     <h4>${v.price}</h4>
//                                 `
//                         }
//                     )
//             }
//         )





// fromEvent(document.querySelector('.input-type-text'), 'keyup')
//     .pipe(
//         map(e => e.target.value),
//         debounceTime(1000),
//         distinct() 
//     )
//     .subscribe(
//         createSubscribeTemplate('debounceTime')
//     )


from([1,2,3,3,3,3,5,5,1,1,98,98,99,99,2,4,6,6,7,10])
        .pipe(
            distinctUntilChanged()
        )
        .subscribe(
            createSubscribeTemplate('from')
        )