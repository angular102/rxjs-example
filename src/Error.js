import {Observable,
    fromPromise, fromEvent,
    merge, of, interval, timer,
    range,
    from} from "rxjs";
import {
    filter,
    map,
    scan,
    take,
    pluck,
    first,
    last,
    find,
    findIndex,
    skip,
    skipWhile,
    takeWhile,
    skipUntil,
    takeUntil, debounceTime, distinct, distinctUntilChanged, buffer,
    bufferTime, bufferCount, defaultIfEmpty, every, tap,
    delay, mergeMap, concatMap, zip, withLatestFrom,
    combineLatest, catchError,
} from 'rxjs/operators'


function createSubscribeTemplate(name) {
    return {
        next(value) {
            console.log(value)
        },
        error(err) {
            console.log(`Error: ${err}`)
        },
        complete() {
            console.log(`${name}: Completed !`)
        }
    }
}





Observable.throw(new Error('error error error'))
    .pipe(
        catchError(error => {
            console.log(error)})
    )
    .subscribe(x => {
        console.log(x)
    })

interval(500)
    .pipe(
        take(2)
    )
    .subscribe(
        createSubscribeTemplate('error')
    )














