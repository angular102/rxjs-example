import {of, from, Observable, fromEvent} from 'rxjs'
import {range, timer, interval} from 'rxjs'
import {scan, map} from "rxjs/operators";


// const stream$ = of(1,2,3,4, 5, 6 ,7)
// const streamTwo$ = of('hello', 'world', 'i', 'am')
//
// stream$.subscribe( val => {
//     console.log('Value:', val)
// })
//
// streamTwo$.subscribe( val => {
//     console.log('Value-string:', val)
// })

// const arr$ = from([1,2,3,4]).pipe(
//     scan((acc, v) => acc.concat(v), [])
// )
// arr$.subscribe(val => console.log(val))




// const stream$ = new Observable( observer => {
//     observer.next('First value')
//
//     setTimeout(() => observer.next('After 1'), 1000)
//
//     setTimeout(() => observer.complete(), 200)
//
//     // setTimeout(() => observer.error('Error test'), 3000)
//
//     setTimeout(() => observer.next('After 5'), 5000)
//
// })

// stream$.subscribe(
//     (val) => console.log('Value:', val),
//     (err) => console.log(err),
//     () => console.log('complete')
// )

// stream$.subscribe({
//     next(val) {
//         console.log(val)
//     },
//     error(err) {
//         console.error(err)
//     },
//     complete() {
//         console.log('complete')
//     }
// })





// fromEvent(document.querySelector('canvas'), 'mousemove')
//     .pipe(
//         map( e => ({
//             x: e.offsetX,
//             y: e.offsetY,
//             ctx: e.target.getContext('2d'),
//         }))
//     )
//     .subscribe( pos => {
//         pos.ctx.fillRect(pos.x, pos.y, 2, 2)
//     })
//
// const clear$ = fromEvent(document.getElementById('clear'), 'click')
//
// clear$.subscribe( () => {
//     const canvas = document.querySelector('canvas')
//     canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
// })
//





// const sub = interval(500).subscribe( v => console.log(v))
//
// setTimeout( () => {
//     console.log('finish')
//     sub.unsubscribe()
// }, 4000)



// timer(2500).subscribe( v => console.log(v))


// range(42, 10).subscribe( v => console.log(v))




