import {Observable,fromPromise, fromEvent, of, interval, timer, range, from} from "rxjs";
import {filter, map, scan, take} from 'rxjs/operators'

function createSubscribeTemplate(name) {
    return {
        next(value) {
            console.log(`${name}: ${value}`)
        },
        error(err) {
            console.log(`Error: ${err}`)
        },
        complete() {
            console.log(`${name}: Completed !`)
        }
    }
}

 function delay(ms = 100) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(ms);
        }, ms)
    })
 }

 // delay(3000).then(() => {
 //     console.log(`Promise was resolved`)
 // })

const p$ = from(delay(4000))
p$.subscribe(createSubscribeTemplate('fromPromise'))